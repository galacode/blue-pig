﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BluePig.WebApi.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Words",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    En = table.Column<string>(type: "TEXT", nullable: false),
                    IsWatched = table.Column<int>(type: "INTEGER", nullable: false),
                    Cn = table.Column<string>(type: "TEXT", nullable: false),
                    HaveMp4 = table.Column<int>(type: "INTEGER", nullable: false),
                    Lines = table.Column<string>(type: "TEXT", nullable: false),
                    Group = table.Column<string>(type: "TEXT", nullable: false),
                    CreateDate = table.Column<string>(type: "TEXT", nullable: false),
                    LastWatchDate = table.Column<string>(type: "TEXT", nullable: false),
                    LastUpdateDate = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Words", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Words");
        }
    }
}
