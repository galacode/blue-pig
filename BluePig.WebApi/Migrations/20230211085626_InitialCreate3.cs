﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BluePig.WebApi.Migrations
{
    public partial class InitialCreate3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Mp4Url",
                table: "Words");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Mp4Url",
                table: "Words",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }
    }
}
