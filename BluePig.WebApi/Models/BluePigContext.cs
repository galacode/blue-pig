﻿using Microsoft.EntityFrameworkCore;

namespace BluePig.WebApi.Models
{
    public class BluePigContext : DbContext
    {
        public DbSet<Word> Words { get; set; }


        public DbSet<Trace> Traces { get; set; }

        public string DbPath { get; }
        public BluePigContext()
        {
            DbPath = Path.Join(AppDomain.CurrentDomain.BaseDirectory, "BluePig.db");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");
    }
}
