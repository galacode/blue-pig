﻿namespace BluePig.WebApi.Models
{

    public class Word
    {
        public int Id { get; set; }
        public string En { get; set; }
        public bool IsWatched { get; set; } = false;
        public bool IKnewIt { get; set; } = false;
        public string Cn { get; set; }
        public bool HaveMp4 { get; set; }
        public string Mp4Url => $@"http://172.26.172.122:18104/mp4s/{En}/{En}.mp4";
        public string? LineName { get; set; }
        public string? LineUrl =>$@"http://172.26.172.122:18104/mp4s/{En}/ts/{LineName}";
        public string Lines { get; set; } = "#";
        public string Group { get; set; } = "英语二";
        public string CreateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        public string? LastWatchDate { get; set; }
        public string LastUpdateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
    }
}

