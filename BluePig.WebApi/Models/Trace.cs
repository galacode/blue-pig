﻿namespace BluePig.WebApi.Models
{
    public class Trace
    {
        public int Id { get; set; }
        public string En { get; set; }
        public bool SoEasy { get; set; }
        public string CreateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
    }
}
