#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 81
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["BluePig/BluePig.csproj", "BluePig/"]
RUN dotnet restore "BluePig/BluePig.csproj"
COPY . .
WORKDIR "/src/BluePig"
RUN dotnet build "BluePig.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "BluePig.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "BluePig.dll"]

#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 82
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["BluePig.WebApi/BluePig.WebApi.csproj", "BluePig.WebApi/"]
RUN dotnet restore "BluePig.WebApi/BluePig.WebApi.csproj"
COPY . .
WORKDIR "/src/BluePig.WebApi"
RUN dotnet build "BluePig.WebApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "BluePig.WebApi.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "BluePig.WebApi.dll"]