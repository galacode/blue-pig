# BluePig

## screenshot

![](/docs/Snipaste_2023-02-20_20-01-37.png)

## build                     
```
dotnet tool install --global dotnet-ef
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.Tools
dotnet ef migrations add InitialCreate
dotnet ef database update
```

## webview2
``` cs
var ok = webView.CoreWebView2.ExecuteScriptAsync("document.querySelector('#app > div.app-nav').style.display='none';");
var ok2 = webView.CoreWebView2.ExecuteScriptAsync("document.querySelector('#app > div.home-page.container > div.search-container').style.display='none';");
var ok3 = webView.CoreWebView2.ExecuteScriptAsync("document.querySelector('#app > div.footer-view').style.display='none';");
var ok4 = webView.CoreWebView2.ExecuteScriptAsync("document.querySelector('#app > div.home-page.container > div.content-box > div.filter-box').style.display='none';");
var ok5 = webView.CoreWebView2.ExecuteScriptAsync("document.querySelector('#app > div.home-page.container > div.content-box > div.filter-box > div').style.display='none';");

```

## 参考
https://learn.microsoft.com/zh-cn/dotnet/communitytoolkit/mvvm/observableobject

