﻿using System.Net.Http;
namespace BluePig.OpenAPIs
{
    public class BluePigHttpClient
    {
        private readonly swaggerClient _bluePigService;
        public BluePigHttpClient(IHttpClientFactory clientFactory)
        {
            var client = clientFactory.CreateClient();
            _bluePigService= new swaggerClient("http://172.26.172.122:18104/", client);
        }

        public swaggerClient GetBluePigService()
        {
            return _bluePigService;
        }
    }
}
