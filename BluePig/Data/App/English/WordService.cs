﻿using BluePig.OpenAPIs;

namespace BluePig.Data.App.ECommerce;

public class WordService
{
    private static List<BluePig.Word> _datas => new swaggerClient("https://localhost:7101/", new HttpClient()).WordsAllAsync().Result.ToList();

    public static List<BluePig.Word> GetGoodsList() => _datas;

    public static List<BluePig.Word> GetRelatedGoodsList() => GetGoodsList().GetRange(0, 10);

    public static List<MultiRangeDto> GetMultiRangeList() => new()
    {
        new(RangeType.All,"All",0),
        new(RangeType.LessEqual,"<= $10",10),
        new(RangeType.Range,"$10 - $100",10,100),
        new(RangeType.Range,"$100 - $500",100,500),
        new(RangeType.MoreEqual,">=$500",500),
    };

    public static List<string> GetCategortyList() => new()
    {
        "饮水机",
        "纯水机",
        "商务机",
        "胶囊机",
        "空净系列",
        "中央处理设备"
    };

    public static List<string> GetBrandList() => new()
    {
        "LONSID"
    };
}

