﻿using BluePig.Data.App.English;
using BluePig.OpenAPIs;
using BluePig.Pages.App.English.Word.ViewModel;
namespace BluePig.Pages.App.English.Word
{
    public partial class Word : ProCompontentBase
    {
        readonly List<MultiRangeDto> _multiRanges =ShopService.GetMultiRangeList();
        readonly List<string> _categories = ShopService.GetCategortyList();
        readonly List<string> _brands = ShopService.GetBrandList();
         WordPage _shopData;// = new(WordService.GetGoodsList());

        [Inject]
        public NavigationManager Nav { get; set; } = default!;
        [Inject]
        public BluePigHttpClient? bluePigHttpClient { get; set; } 

        protected override void OnInitialized()
        {
            _shopData = new(bluePigHttpClient.GetBluePigService().WordsAllAsync().Result
                .ToList());
            _shopData.MultiRange = _multiRanges[0];
        }

      

        private void NavigateToDetails(int id)
        {
            Nav.NavigateTo($"/app/word/details/{id}");
        }

        private void NavigateToOrder()
        {
            Nav.NavigateTo($"app/ecommerce/order");
        }
    }
}
