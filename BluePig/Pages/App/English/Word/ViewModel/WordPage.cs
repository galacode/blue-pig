﻿using BluePig.Pages.App.ECommerce.Shop;

namespace BluePig.Pages.App.English.Word.ViewModel;

public class WordPage
{
    public List<BluePig.Word> Datas { get; set; }

    private IEnumerable<BluePig.Word> GetFilterDatas()
    {
        IEnumerable<BluePig.Word> datas = Datas;

       

        if (datas.Count() < (PageIndex - 1) * PageSize) PageIndex = 1;

        return datas;
    }

    public List<BluePig.Word> GetPageDatas()
    {
        
        return GetFilterDatas().Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
    }

    public int CurrentCount => GetFilterDatas().Count();

    public int PageIndex { get; set; } = 1;

    public int PageSize { get; set; } = 6;

    public int PageCount => (int)Math.Ceiling(CurrentCount / (double)PageSize);

    public MultiRangeDto? MultiRange { get; set; }

    public string? Category { get; set; }

    public string? Brand { get; set; }

    SortType SortType { get; set; }

    public StringNumber SortTypeLable
    {
        get => SortType.ToString();
        set { SortType = (SortType)Enum.Parse(typeof(SortType), value.ToString()); }
    }

    public string? Search { get; set; }

    public WordPage(List<BluePig.Word> datas)
    {
        Datas = datas;
    }

    public BluePig.Word GetGoods(string? id)
    {
        return Datas.FirstOrDefault(a => a.Id.ToString() == id) ?? Datas.First();
    }
}






