﻿// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using BluePig.Wpf.ViewModels;
using Microsoft.Web.WebView2.Core;
using Wpf.Ui.Controls.Navigation;

namespace BluePig.Wpf.Views.Pages;

/// <summary>
/// Interaction logic for DashboardPage.xaml
/// </summary>
public partial class LearningPage : INavigableView<LearningPageViewModel>
{

    public LearningPageViewModel ViewModel
    {
        get;
    }

    public LearningPage(LearningPageViewModel viewModel)
    {
        ViewModel = viewModel;
        DataContext = this;

        InitializeComponent();
        webView.NavigationStarting += EnsureHttps;
    }

    void EnsureHttps(object sender, CoreWebView2NavigationStartingEventArgs args)
    {
        
    }
    private void WebView_OnNavigationCompleted(object? sender, CoreWebView2NavigationCompletedEventArgs e)
    {
        var ok = webView.CoreWebView2.ExecuteScriptAsync("document.querySelector('#app > div.app-nav').style.display='none';");
        var ok2 = webView.CoreWebView2.ExecuteScriptAsync("document.querySelector('#app > div.home-page.container > div.search-container').style.display='none';");
        var ok3 = webView.CoreWebView2.ExecuteScriptAsync("document.querySelector('#app > div.footer-view').style.display='none';");
        var ok4 = webView.CoreWebView2.ExecuteScriptAsync("document.querySelector('#app > div.home-page.container > div.content-box > div.filter-box').style.display='none';");
        var ok5 = webView.CoreWebView2.ExecuteScriptAsync("document.querySelector('#app > div.home-page.container > div.content-box > div.filter-box > div').style.display='none';");
    }

    private void WebView_OnContentLoading(object? sender, CoreWebView2ContentLoadingEventArgs e)
    {
        //var scriptResult = webView.ExecuteScriptAsync("document.getElementById('app').style.visibility='hidden';document.querySelector(\"#app > div.home-page.container > div.content-box > div.search-result-box > div\").style.visibility='visible';");

    }

    private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
    {
        
    }
}
