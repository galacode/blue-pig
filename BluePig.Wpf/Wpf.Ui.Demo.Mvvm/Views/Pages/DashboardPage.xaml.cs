﻿// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using BluePig.Wpf.ViewModels;
using LiveCharts;
using LiveCharts.Wpf;
using Microsoft.Web.WebView2.Core;
using Wpf.Ui.Controls.Navigation;

namespace BluePig.Wpf.Views.Pages;

/// <summary>
/// Interaction logic for DashboardPage.xaml
/// </summary>
public partial class DashboardPage : INavigableView<DashboardViewModel>
{
    public Func<ChartPoint, string> PointLabel { get; set; }
    public string[] Labels { get; set; }
    public Func<double, string> Formatter { get; set; }
    public DashboardViewModel ViewModel
    {
        get;
    }

    public DashboardPage(DashboardViewModel viewModel)
    {
        ViewModel = viewModel;
        DataContext = this;

        InitializeComponent();
        PointLabel = chartPoint =>
            string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation);
        Labels = new[] { "Got It", "See It" ,"See Got"};
    }
    private void Chart_OnDataClick(object sender, ChartPoint chartpoint)
    {
        var chart = (LiveCharts.Wpf.PieChart)chartpoint.ChartView;

        //clear selected slice.
        foreach (PieSeries series in chart.Series)
            series.PushOut = 0;

        var selectedSeries = (PieSeries)chartpoint.SeriesView;
        selectedSeries.PushOut = 8;
    }


    
}
