﻿// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Net.Http;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using Windows.Media.Protection.PlayReady;

namespace BluePig.Wpf.ViewModels;

public partial class DashboardViewModel : ObservableObject
{
    [ObservableProperty] private int _learnedCount;
    [ObservableProperty] private int _easydCount = 1;
    [ObservableProperty] private int _hardCount = 2;
    [ObservableProperty] private SeriesCollection _seriesCollection;
    [ObservableProperty] private SeriesCollection _pieSeriesCollection;
    [ObservableProperty] private SeriesCollection _knownSeriesCollection;
    [ObservableProperty] private string[] _labels;
    [ObservableProperty] private Func<double, string> _yFormatter;
    private ICollection<Trace> _traces;
    private List<Word> _words = null!;

    private swaggerClient _client = null!;
    public DashboardViewModel()
    {

        GetAllWords();

        InitTrace();
        InitWeekChart();
        InitTodayTarget();
        InitEasyHardPie();
        InitDateGroup();
    }
    [RelayCommand]
    private void Init()
    {
        GetAllWords();
        InitTrace();
        InitWeekChart();
        InitTodayTarget();
        InitEasyHardPie();
        InitDateGroup();
    }

    private void GetAllWords()
    {
        _client = new swaggerClient("http://localhost:18104/", new HttpClient());

        _words = (_client.WordsAllAsync().Result).ToList();
    }

    private void InitTrace()
    {
         _traces = _client.TracesAllAsync().Result;
    }

    private void InitWeekChart()
    {
       
        var weekTraces = _client.TracesAllAsync().Result.ToList().Where(r => DateTime.Parse(r.CreateDate) >= DateTime.Now.AddDays(-6));
        SeriesCollection = new SeriesCollection()
        {
            new LineSeries
            {
                Title = "So Easy",
                Values = new ChartValues<int>(),
            },
            new LineSeries
            {
                Title = "Next Time",
                Values = new ChartValues<int>(),
                PointGeometry = null
            },
        };
        SeriesCollection[0].Values.Clear();
        SeriesCollection[1].Values.Clear();
        var week = new[] { DateTime.Now.AddDays(-9).DayOfWeek };
        _labels = new[] { DateTime.Now.AddDays(-6).DayOfWeek.ToString(), 
            DateTime.Now.AddDays(-5).DayOfWeek.ToString(), 
            DateTime.Now.AddDays(-4).DayOfWeek.ToString(),
            DateTime.Now.AddDays(-3).DayOfWeek.ToString(),
            DateTime.Now.AddDays(-2).DayOfWeek.ToString(),
            DateTime.Now.AddDays(-1).DayOfWeek.ToString(),
            DateTime.Now.AddDays(0).DayOfWeek.ToString()};
        //_yFormatter = value => value.ToString("C");
        SeriesCollection[0].Values
            .Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-6).DayOfWeek && r.SoEasy));
        SeriesCollection[0].Values
            .Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-5).DayOfWeek && r.SoEasy));
        SeriesCollection[0].Values
            .Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-4).DayOfWeek && r.SoEasy));
        SeriesCollection[0].Values.Add(weekTraces.Count(r =>
            DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-3).DayOfWeek && r.SoEasy));
        SeriesCollection[0].Values.Add(weekTraces.Count(r =>
            DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-2).DayOfWeek && r.SoEasy));
        SeriesCollection[0].Values
            .Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-1).DayOfWeek && r.SoEasy));
        SeriesCollection[0].Values.Add(weekTraces.Count(r =>
            DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(0).DayOfWeek && r.SoEasy));

        SeriesCollection[1].Values.Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-6).DayOfWeek && !r.SoEasy));
        SeriesCollection[1].Values.Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-5).DayOfWeek && !r.SoEasy));
        SeriesCollection[1].Values.Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-4).DayOfWeek && !r.SoEasy));
        SeriesCollection[1].Values.Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-3).DayOfWeek && !r.SoEasy));
        SeriesCollection[1].Values.Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-2).DayOfWeek && !r.SoEasy));
        SeriesCollection[1].Values.Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(-1).DayOfWeek && !r.SoEasy));
        SeriesCollection[1].Values.Add(weekTraces.Count(r => DateTime.Parse(r.CreateDate).DayOfWeek == DateTime.Now.AddDays(0).DayOfWeek && !r.SoEasy));
    }

    private void InitTodayTarget()
    {
        LearnedCount = _words.Count(r => DateTime.Parse(r.LastUpdateDate).Day == DateTime.Now.Day);
    }

    private void InitEasyHardPie()
    {
        var client = new swaggerClient("http://localhost:18104/", new HttpClient());
        var traces = client.TracesAllAsync().Result;
        EasydCount = traces.Count(r => DateTime.Parse(r.CreateDate).Day == DateTime.Now.Day && r.SoEasy);
        HardCount = traces.Count(r => DateTime.Parse(r.CreateDate).Day == DateTime.Now.Day && !r.SoEasy);
        PieSeriesCollection = new SeriesCollection
        {
            new PieSeries
            {
                Title = "Easy",
                Values = new ChartValues<ObservableValue> { new ObservableValue(EasydCount) },
                DataLabels = true
            },
            new PieSeries
            {
                Title = "Hard",
                Values = new ChartValues<ObservableValue> { new ObservableValue(HardCount) },
                DataLabels = true
            }
        };
    }

    private void InitDateGroup()
    {
        //var weekKnownCount = _traces.Count(r => DateTime.Parse(r.CreateDate) >= DateTime.Now.AddDays(-6)&&r.SoEasy);
        //var weekUnKnownCount = _traces.Count(r => DateTime.Parse(r.CreateDate) >= DateTime.Now.AddDays(-6)&&!r.SoEasy);
        var allEasyCount = _words.Count(r => r.IKnewIt);
        var allHardCount = _words.Count(r => !r.IKnewIt);
        var watchCount = _words.Count(r => r.IsWatched);
        var unWatchCount = _words.Count(r => !r.IsWatched);
        KnownSeriesCollection = new SeriesCollection
        {
            new StackedColumnSeries
            {
                Values = new ChartValues<double> {allEasyCount,watchCount},
                StackMode = StackMode.Values, // this is not necessary, values is the default stack mode
                DataLabels = true,
                Title = "Yes"
            },
            new StackedColumnSeries
            {
                Values = new ChartValues<double> {allHardCount,unWatchCount},
                StackMode = StackMode.Values,
                DataLabels = true,
                Title = "No"
            }
        };
    }
}
