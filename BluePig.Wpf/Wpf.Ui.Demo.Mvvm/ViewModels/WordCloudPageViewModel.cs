﻿// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Net.Http;
using Microsoft.Web.WebView2.Wpf;

namespace BluePig.Wpf.ViewModels;

public partial class WordCloudPageViewModel : ObservableObject
{
    [ObservableProperty] private int _reviewCount;
    [ObservableProperty] private int _needReviewCount;
    [ObservableProperty] private string? _en = "今日任务已完成";

    [ObservableProperty] private string _cn ;

    private Word _word = null!;
    private List<Trace> _needReviewWords = null!;
    private List<Trace> _todayViewWords = null!;
    [ObservableProperty]
    private string _url = "https://33.agilestudio.cn/?subtitleKeyword=word&exactFlag=0&phraseMode=0&page=1&limit=20";

    private List<Word> _words = null!;

    private swaggerClient _client = null!;

    public WordCloudPageViewModel()
    {
        Url = $@"{AppDomain.CurrentDomain.BaseDirectory}Htmls\wordCloud.html";
        //Init();
    }


    [RelayCommand]
    private async Task NextWord(WebView2 r)
    {
        if (ReviewCount < _needReviewWords.Count)
        {
            await TraceNotEasy();
            await GetTodayWactchedWord();
            await GetWatchedWord();
            Next();
        }

    }

    [RelayCommand]
    private async Task SoEasy()
    {
        if (ReviewCount < _needReviewWords.Count)
        {
            await TraceEasy(); 
            TooEasy();
            await GetTodayWactchedWord();
            await GetWatchedWord();
            Next();
        }
    }

    private async Task Init()
    {
        _client = new swaggerClient("http://localhost:18104/", new HttpClient());

        _words = (await _client.WordsAllAsync()).ToList();
        await GetNeedReviewWords();
        await GetTodayWactchedWord();

        Next();
    }

    private async Task GetNeedReviewWords()
    {
        _needReviewWords = (await _client.TracesAllAsync())
            .Where(r =>
                r.SoEasy == false
                && (DateTime.Parse(r.CreateDate).Date == DateTime.Now.AddDays(-2).Date
                    || DateTime.Parse(r.CreateDate).Date == DateTime.Now.AddDays(-4).Date
                    || DateTime.Parse(r.CreateDate).Date == DateTime.Now.AddDays(-7).Date
                    || DateTime.Parse(r.CreateDate).Date == DateTime.Now.AddDays(-15).Date))
            .GroupBy(r => new { r.En })
            .Select(r => r.First())
            .ToList();
        NeedReviewCount = _needReviewWords.Count;
    }

    private void Next()
    {
        if (ReviewCount< _needReviewWords.Count)
        {
            En = _needReviewWords.First(r => _todayViewWords.Count(t => t.En == r.En) == 0).En;
            Cn = _words.First(r => r.En == En).Cn;
            _word = _words.First(r => r.En == En);
            Url = $@"https://33.agilestudio.cn/?subtitleKeyword={En}&exactFlag=0&phraseMode=0&page=1&limit=20";
        }
    }

    private async Task GetWatchedWord()
    {
        _word.IsWatched = true;
        await _client.Words3Async(_word.Id, _word);
    }

    private void TooEasy()
    {
        _word.IKnewIt = true;
    }

    private async Task TraceEasy()
    {
        await _client.TracesAsync(new Trace() { En = _en,SoEasy = true});
        
    }

    private async Task GetTodayWactchedWord()
    {
        _todayViewWords = (await _client.TracesAllAsync())
            .Where(r =>(DateTime.Parse(r.CreateDate).Date == DateTime.Now.Date))
            .GroupBy(r => new { r.En })
            .Select(r => r.First())
            .ToList();
        ReviewCount = 0;
        foreach (var t1 in _needReviewWords)
        {
            foreach (var t in _todayViewWords)
            {
                if (t.En== t1.En)
                    ReviewCount++;
            }
        }
        //ReviewCount = _todayViewWords.Count;
    }

    private async Task TraceNotEasy()
    {
        await _client.TracesAsync(new Trace() { En = _en,SoEasy = false});
    }
}
